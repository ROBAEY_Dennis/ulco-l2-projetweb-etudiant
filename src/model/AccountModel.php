<?php


namespace model;


class AccountModel
{
    static function check($firstname, $lastname, $mail, $password): bool
    {
        if (strlen($firstname)<2 || strlen($lastname)<2 || !filter_var($mail, FILTER_VALIDATE_EMAIL) || strlen($password)<6) {
            return false;
        }

        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT * FROM account WHERE account.mail= ?";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(array($mail));

        if (!count($req->fetchAll())==0) {
            return false;
        }

        return true;
    }

    static function signin($firstname, $lastname, $mail, $password): bool
    {
        if (!self::check($firstname, $lastname, $mail, $password)) {
            return false;
        }

        $db = \model\Model::connect();
        $sql = "INSERT INTO account(firstname, lastname, mail, password) VALUES (?,?,?,?)";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(array($firstname,$lastname,$mail,password_hash($password,PASSWORD_DEFAULT)));

        return true;
    }

    static function login($mail,$password): array
    {
        $db = \model\Model::connect();
        $sql = "SELECT * FROM account WHERE account.mail=?";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(array($mail));
        $tab=$req->fetchAll();

        if (password_verify($password,$tab[0]["password"])) {
            return $tab;
        } else {
            return array ();
        }
    }

    static function update($firstname,$lastname,$new_mail,$current_mail, $id): bool
    {
        $db = \model\Model::connect();

        if ($current_mail!=$new_mail) { //si les mails sont différents on regarde si il n'existe pas déjà un utilisateur avec la même adresse mail
            $sql= "SELECT * FROM account WHERE account.mail=?";
            $req = $db->prepare($sql);
            $req->execute(array($new_mail));

            if (count($req->fetchAll())!=0) {
                return false;
            }
        }


        $sql = "UPDATE account SET firstname=?,lastname=?,mail=? WHERE account.id=?";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(array($firstname,$lastname,$new_mail,$id));
        return true;
    }

    static function updateWallet($id_account,$montant): void
    {
        $db = \model\Model::connect();
        $sql = "UPDATE account SET wallet=? WHERE account.id=?";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(array($montant,$id_account));
    }
}