<?php


namespace model;


class CartModel
{
    //TODO : create table cart (id_user INT, id_product INT, quantity INT)
    static function listCart($id_user): array
    {
        $db = \model\Model::connect();

        $sql = "SELECT cart.id_product, cart.quantity, product.image, product.price, product.promotion, category.name AS category_name, product.name AS product_name FROM cart 
                INNER JOIN product, category WHERE cart.id_user=? AND cart.id_product=product.id AND product.category=category.id;"; //TODO : ?
        $req = $db->prepare($sql);
        $req->execute(array($id_user));
        return $req->fetchAll();
    }

    static function add($id_user,$id_product,$quantity): void
    {
        $db = \model\Model::connect();

        //on trouve la quantité pour ne pas dépasser 5 par produit

        $sql = "SELECT quantity FROM cart WHERE id_user=? AND id_product=?";

        $req = $db->prepare($sql);
        $req->execute(array($id_user,$id_product));
        $quantityTab = $req->fetchAll();

        if (!empty($quantityTab[0])) { //si le produit a déjà était ajouté, il ne faut que le modifier
            $quantityFromDB=$quantityTab[0]['quantity'];

            if ($quantityFromDB+$quantity>=5) {
                $quantity=5;
            } else {
                $quantity+=$quantityFromDB;
            }

            $sql = "UPDATE cart SET quantity=? WHERE cart.id_user=? AND cart.id_product=?";

            // Exécution de la requête
            $req = $db->prepare($sql);
            $req->execute(array($quantity,$id_user,$id_product));

        } else { //sinon, il faut l'ajouter
            $sql = "INSERT INTO cart(id_user, id_product, quantity) VALUES (?,?, ?)";

            $req = $db->prepare($sql);
            $req->execute(array($id_user,$id_product,$quantity));
        }
    }

    static function update($id_user,$products): void
    {
        $db = \model\Model::connect();

        foreach ($products as $id_product => $quantity) {
            if ($quantity!=0) {
                $sql = "UPDATE cart SET quantity=? WHERE cart.id_user=? AND cart.id_product=?";

                $req = $db->prepare($sql);
                $req->execute(array($quantity,$id_user,$id_product));
            } else { //si la quantité est de 0 : on supprime le produit du panier dans la BDD
                $sql = "DELETE FROM cart WHERE cart.id_user=? AND cart.id_product=?";

                $req = $db->prepare($sql);
                $req->execute(array($id_user,$id_product));
            }
        }
    }

    static function order($id_account, $products): void
    {
        $db = \model\Model::connect();

        foreach ($products as $id_product => $quantity) { //on ajoute le produit dans les commandes, puis on le supprime du panier
            if ($quantity!=0) { //si le produit a une quantité de 0, il ne faut que le supprimer du panier
                $sql = "SELECT quantity FROM orders WHERE id_account=? AND id_product=?";

                $req = $db->prepare($sql);
                $req->execute(array($id_account,$id_product));
                $quantityTab = $req->fetchAll();

                if (!empty($quantityTab[0])) { //si le produit a déjà était ajouté, il ne faut que le modifier
                    $quantityFromDB=$quantityTab[0]['quantity'];

                    $sql = "UPDATE orders SET quantity=? WHERE orders.id_account=? AND orders.id_product=?";

                    // Exécution de la requête
                    $req = $db->prepare($sql);
                    $req->execute(array($quantityFromDB+$quantity,$id_account,$id_product));

                } else { //sinon, il faut l'ajouter
                    $sql = "INSERT INTO orders(id_account,id_product,quantity) VALUES (?,?,?)";

                    $req = $db->prepare($sql);
                    $req->execute(array($id_account, $id_product, $quantity));
                }
            }

            $sql = "DELETE FROM cart WHERE cart.id_user=? AND cart.id_product=?"; //dans tout les cas, on supprime le produit du panier

            $req = $db->prepare($sql);
            $req->execute(array($id_account,$id_product));
        }
    }

    static function listOrder($id_user): array
    {
        $db = \model\Model::connect();

        $sql = "SELECT orders.id_product, orders.quantity, product.image, product.price, product.promotion, category.name AS category_name, product.name AS product_name FROM orders 
                INNER JOIN product, category WHERE orders.id_account=? AND orders.id_product=product.id AND product.category=category.id;"; //TODO : ?
        $req = $db->prepare($sql);
        $req->execute(array($id_user));
        return $req->fetchAll();
    }
}