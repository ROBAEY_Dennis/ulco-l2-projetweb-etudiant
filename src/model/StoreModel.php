<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function listProduct(): array
  {
      $db = \model\Model::connect();

      $sql = "SELECT p.id, p.name, p.price, p.promotion, p.image, c.name AS category_name FROM product  AS p INNER JOIN category c WHERE p.category = c.id";
      $req = $db->prepare($sql);
      $req->execute();
      return $req->fetchAll();
  }

  static function infoProduct(int $id): array
  {
      $db = \model\Model::connect();

      $sql = "SELECT p.id, p.name, p.price, p.promotion, p.image, p.image_alt1, p.image_alt2, p.image_alt3, p.spec, c.name AS category_name FROM product AS p INNER JOIN category c WHERE p.category = c.id AND p.id=$id";
      $req = $db->prepare($sql);
      $req->execute();
      return $req->fetchAll();
  }

  static function listSpecificProduct($research,$categorie, $prix): array
  {
      $db = \model\Model::connect();
      $sql = "SELECT p.id, p.name, p.price, p.promotion, p.image, c.name AS category_name FROM product  AS p INNER JOIN category c WHERE p.category = c.id ";

      if ($categorie!=""){
          $sql.=" AND (";
          foreach ($categorie as $c) {
              if ($c==$categorie[0]) {
                  $sql.=' c.name LIKE "' . $c . '"';
              } else {
                  $sql.=' OR c.name LIKE "' . $c . '"';
              }
          }
          $sql.=") ";
      }

      if ($research!=""){
            $sql.=' AND p.name LIKE ?';
      }

      if($prix!="") {
          if ($prix=="croissant") {
              $sql.=" ORDER BY p.price ASC";
          } else {
              $sql.=" ORDER BY p.price DESC";
          }
      }

      $req = $db->prepare($sql);
      if ($research!="") {
          $req->execute(array("%" . $research . "%"));
      } else {
          $req->execute();
      }

      return $req->fetchAll();

  }

}