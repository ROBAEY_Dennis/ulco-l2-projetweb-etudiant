<?php if (!empty($_SESSION['user'])) { ?>

    <div style="margin: 30px">
        Le montant de votre porte monnaie s'élève à <?=$_SESSION['user']['wallet']?> €.
    </div>
    <form style=" width: 30%" method="post" action="/account/wallet/update">
        <div id="cart-footer">
        <label>Modifier le montant</label>
            <input type="number" name="montant"/>
            <div><br/></div>
            <button type="submit">Mettre à jour le porte monnaie</button>
        </div>
    </form>

<?php } else {
    header("Location: /account?status=disconnected");
}?>