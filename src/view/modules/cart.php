<?php if (!empty($_GET)) {
    if ($_GET['status']=='fonds_insuffisants') {
        echo "<div class='box error' style='margin-left: 50px'>Fonds insuffisants.</div>";
    }
} ?>

<h1 style="margin: 20px 30px">Panier</h1>

<div id="cart">
<form method="post" action="/cart/update">
<?php
    $sum=0;
    foreach($params['products'] as $product) { ?>

        <div class="cart-product">
            <p class="card-image"><img src="/public/images/<?= $product['image'] ?>"/> </p>

            <div style="width: 400px" >
                <p class="card-category"><?=$product['category_name']?></p>
                <p class="card-title"><a href="/store/<?= $product['id_product']?>"><?= $product['product_name']?></a></p>
                <?php if ($product['promotion']) { ?>
                    <p><span style="color: red">PROMOTION !!!</span></p>
                <?php } ?>
            </div>
            <div class="cart-quantity">
                <p class="quantity">Quantité : </p>
                <button type="button">-</button>
                <button type="button"><?= $product['quantity']?></button>
                <button type="button">+</button>
                <input type="hidden" name="<?= $product['id_product']?>" value="<?= $product['quantity']?>"/>
            </div>
            <div class="product-price-div" style="margin-left: 30px">
                <p class="card-price">Prix unitaire : </p>
                <?php if ($product['promotion']) {
                    $sum+=($product['price']/2)*$product['quantity']; ?>
                    <p class="price"><span class="product-price"><?= $product['price']/2 ?>€</p>
                <?php } else { ?>
                    <p class="price"><span class="product-price"><?= $product['price']?></span>€</p>
                <?php $sum+=$product['price']*$product['quantity']; } ?>
            </div>

        </div>

<?php } ?>
    <div id="cart-bottom" style="display: flex">
        <div style="width: 900px"></div>
        <div>
            <p class="card-price">Prix total du panier : </p>
            <p class="price"><span id="total-price"><?=$sum?></span> €</p>
        </div>
    </div>
    <div id="cart-footer">
        <button type="submit" formaction="/cart/order">Procéder au paiement</button>
        <button type="submit">Mettre à jour le panier</button>
    </div>
</form>
</div>
<script src="/public/scripts/cart.js"></script>