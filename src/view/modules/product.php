<div id="product">
    <div>
        <div class="product-images">
            <img src="/public/images/<?=$params["infoProduct"][0]["image"]?>" />
            <div class="product-miniatures">
                <div>
                    <img src="/public/images/<?=$params["infoProduct"][0]["image"]?>"/>
                </div>
                <div>
                    <img src="/public/images/<?=$params["infoProduct"][0]["image_alt1"]?>"/>
                </div>
                <div>
                    <img src="/public/images/<?=$params["infoProduct"][0]["image_alt2"]?>"/>
                </div>
                <div>
                    <img src="/public/images/<?=$params["infoProduct"][0]["image_alt3"]?>"/>
                </div>
            </div>
        </div>
        <div class="product-infos">
            <p class="product-category">
                <?=$params["infoProduct"][0]["category_name"]?>
            </p>
            <h1>
                <?=$params["infoProduct"][0]["name"]?>
                <?php if ($params["infoProduct"][0]["promotion"]) {
                    echo "<span style='color: red'> PROMOTION !!!</span>";
                }
                ?>
            </h1>
            <p class="product-price">
                <?php if ($params["infoProduct"][0]["promotion"]) {
                    echo "<strike>" . $params["infoProduct"][0]["price"] . "</strike> " . $params["infoProduct"][0]["price"]/2;
                } else {
                    echo $params["infoProduct"][0]["price"];
                }
                ?>
            </p>
            <form method="post" action="/cart/add">
                <button type="button">-</button>
                <button type="button">1</button>
                <button type="button">+</button>
                <input type="hidden" name="<?= $params["infoProduct"][0]['id']?>" value="1"/>
                <input type="submit"/>
            </form>
        </div>
    </div>
    <div>
        <div class="product-spec">
            <h2>Spécificités</h2>
            <?= $params["infoProduct"][0]["spec"]?>
        </div>
        <div class="product-comments">
            <h2>Avis</h2>
            <?php if (empty($params["comment"][0])) { ?>
            <p>Il n'y a pas d'avis pour ce produit.</p>
            <?php } else {
                foreach ($params["comment"] as $comment) { ?>
                    <form method='post' action="<?= "/store/" . $params["infoProduct"][0]["id"] . "/deleteComment"?>">
                    <div class='product-comment'>
                        <p class='product-comment-author'> <?= $comment['firstname'] . " " . $comment['lastname'] ?></p>
                        <p style="display: flex">
                            <?=$comment['content']?>
                            <?php if (!empty($_SESSION)) {
                                if ($comment['id_account'] == $_SESSION['user']['id']) { ?>
                                     <input type="hidden" name="id" value="<?= $comment["id"]?>" />
                                     <span style="display: flex; flex: 1;" ></span >
                                     <label >
                                         <button type = "submit" >
                                            <i class="fas fa-trash" ></i >
                                         </button >
                                     </label >
                                <?php }
                            } ?>
                        </p>
                    </div>
                    </form>
                <?php }
            }
            if (!empty($_SESSION['user'])) { ?>
                <form method="post" action="<?= "/store/" . $params["infoProduct"][0]["id"] . "/postComment"?>">
                    <input type="text" name="comment" placeholder="Rédigez un commentaire">
                </form>
            <?php } else { ?>
                <div>Veuillez-vous <a style="text-decoration: underline" href="/account">connecter</a> pour publier un commentaire.</div>
            <?php } ?>
        </div>
    </div>
</div>
<script src="/public/scripts/product.js"></script>