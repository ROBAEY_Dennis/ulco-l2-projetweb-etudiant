<nav>
    <img src="/public/images/logo.jpeg"/>
    <a href="/">Accueil</a>
    <a href="/store">Boutique</a>
    <?php if (empty($_SESSION['user'])) { ?>
        <a class="account" href="/account"><img src="/public/images/avatar.png"/>Compte</a>
    <?php } else { ?>

        <a class="account" href="/account/infos"><img src="/public/images/avatar.png"/><?= $_SESSION['user']['firstname'] . " " . $_SESSION['user']['lastname'] ?></a>
        <a href="/account/wallet"><?=$_SESSION['user']['wallet']?>€ <i class="fas fa-coins"></i></a>
        <a href="/cart">Panier</a>
        <a href="/account/logout">Déconnexion</a>
    <?php }  ?>
</nav>