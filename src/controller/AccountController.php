<?php


namespace controller;

use model\AccountModel;
use model\CartModel;
use model\Model;
use model\StoreModel;
use view\Template;

class AccountController
{

    public function account(): void
    {
        $params= [
            "title"  => "Account",
            "module" => "account.php"
        ];
        Template::render($params);
    }

    public function signin(): void
    {
        $firstname=htmlspecialchars($_POST['userfirstname']);
        $lastname=htmlspecialchars($_POST['userlastname']);
        $mail=htmlspecialchars($_POST['usermail']);
        $password=htmlspecialchars($_POST['userpass']);

        if (\model\AccountModel::signin($firstname, $lastname, $mail, $password)) {
            header("Location: /account?status=signin_success");
        } else {
            header("Location: /account?status=signin_fail");
        }
    }

    public function login(): void
    {
        $mail=$_POST['usermail'];
        $password=$_POST['userpass'];

        $user=\model\AccountModel::login($mail,$password);

        if (count($user)>0){
            $userSession = array(
                "id" => $user[0]["id"],
                "firstname" => $user[0]["firstname"],
                "lastname" => $user[0]["lastname"],
                "mail" => $user[0]["mail"],
                "wallet" => $user[0]["wallet"],
            );

            $_SESSION["user"]=$userSession;

            header("Location: /store");
        } else {
            header("Location: /account?status=login_fail");
        }
    }

    public function logout(): void
    {
        session_destroy();
        header("Location: /account?status=logout");
    }

    public function infos(): void
    {
        if (empty($_SESSION['user'])) {
            header("Location: /account?status=disconnected");
            exit();
        }

        $ordered_products=CartModel::listOrder($_SESSION['user']['id']);

        $params= [
            "title"  => "Infos",
            "module" => "infos.php",
            "ordered_products" => $ordered_products,
        ];
        Template::render($params);
    }

    public function update(): void
    {
        $firstname=htmlspecialchars($_POST['firstname']);
        $lastname=htmlspecialchars($_POST['lastname']);
        $new_mail=htmlspecialchars($_POST['mail']);
        $id=$_SESSION['user']['id'];

        $current_mail=$_SESSION["user"]["mail"];

        //on regardera si le nouveau mail (si l'utilisateur l'a modifié) que il ne soit pas déjà utilisé

        if (AccountModel::update($firstname,$lastname,$new_mail,$current_mail, $id)) {
            $_SESSION['user']['firstname']=$firstname;
            $_SESSION['user']['lastname']=$lastname;
            $_SESSION['user']['mail']=$new_mail;

            header("Location: /account/infos?status=update_success");
            exit();
        } else {
            header("Location: /account/infos?status=email_already_used");
            exit();
        }
    }

    public function wallet(): void
    {
        $params= [
            "title"  => "Wallet",
            "module" => "wallet.php"
        ];

        Template::render($params);
    }

    public function walletUpdate(): void
    {

        $id_account=$_SESSION['user']['id'];

        if (empty($_POST['montant']) || $_POST['montant']<=0) {
            $montant=0;
        } else {
            $montant=$_POST["montant"];
        }

        AccountModel::updateWallet($id_account,$montant);

        $_SESSION['user']['wallet']=$montant;

        header("Location: /account/wallet?status=update_success");
    }
}