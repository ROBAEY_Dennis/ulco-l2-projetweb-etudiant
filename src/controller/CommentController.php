<?php


namespace controller;


use model\CommentModel;

class CommentController
{
    public function postComment($id_product): void
    {
        $id_account=$_SESSION['user']['id'];

        $content=htmlspecialchars($_POST['comment']);

        CommentModel::insertComment($content,$id_product,$id_account);


        header("Location: /store/$id_product");
        exit();
    }

    public function deleteComment($id_product): void
    {
        $id_comment=$_POST['id'];

        CommentModel::deleteComment($id_comment);

        header("Location: /store/$id_product");
        exit();
    }
}