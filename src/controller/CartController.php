<?php


namespace controller;


use model\AccountModel;
use model\CartModel;

class CartController
{
    public function cart(): void
    {
        if (empty($_SESSION['user'])){
            header("Location: /account?status=disconnected");
            exit();
        } else {
            $products=\model\CartModel::listCart($_SESSION['user']['id']);

            // Variables à transmettre à la vue
            $params = array(
                "title" => "Cart",
                "module" => "cart.php",
                "products" => $products
            );

            \view\Template::render($params);
        }
    }

    public function addProduct(): void
    {
        foreach ($_POST as $id => $quantite) { //il n'y aura qu'un seul tour (pour accéder à la clé(id produit) + la valeur(quantité)
            $quantity=$quantite;
            $id_product=$id;
        }

        CartModel::add($_SESSION['user']['id'],$id_product,$quantity);
        header("Location: /cart?status=productAdded");
    }

    public function update(): void
    {
        CartModel::update($_SESSION['user']['id'],$_POST);
        header("Location: /cart?status=updated");
        exit();
    }

    public function order(): void
    {
        CartModel::update($_SESSION['user']['id'],$_POST); //on met d'abord à jour le panier si besoin

        $products=\model\CartModel::listCart($_SESSION['user']['id']);

        $cart_total_price=0;

        foreach ($products as $product) {
            if  ($product['promotion']) {
                $cart_total_price+=($product['price']/2)*$product['quantity'];
            } else {
                $cart_total_price+=$product['price']*$product['quantity'];
            }
        }

        if ($cart_total_price>$_SESSION['user']["wallet"]) {  //si l'utilisateur n'a pas assez d'argent
            header("Location: /cart?status=fonds_insuffisants");
            exit();
        }

        CartModel::order($_SESSION['user']['id'],$_POST);

        $_SESSION['user']["wallet"]-=$cart_total_price;
        AccountModel::updateWallet($_SESSION['user']['id'],$_SESSION['user']["wallet"]);
        header("Location: /account/infos?status=order_success");
        exit();
    }
}