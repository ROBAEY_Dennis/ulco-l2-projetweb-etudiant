document.addEventListener("DOMContentLoaded",function (){
    let display_image=document.querySelector("div.product-images img")
    let div_minia=document.querySelector("div.product-miniatures")
    let miniatures=div_minia.getElementsByTagName("div")

    for (let miniature of miniatures) {
        miniature.addEventListener("click",function () {
            display_image.src=miniature.querySelector("img").src
        })
    }

    let products = document.querySelectorAll("form button")
    let button_submit = document.querySelector("input[type='hidden']") //input hidden qui contient la paire clé(id du produit) => valeur(quantité)
    let nb=1

    products[1].innerHTML=nb;

    let div_info = document.querySelector("div.product-infos")
    let box_error = document.createElement("div")
    box_error.innerHTML="<div class='box error'> Quantité maximale autorisée  ! </div>"


    products[0].addEventListener("click",function (){
        if (nb!==1) {
            if (nb===5) {
                if (div_info.contains(box_error)) {
                    div_info.removeChild(box_error)
                }
            }
            nb--
            products[1].innerHTML=nb
            button_submit.value=nb
        }
    })

    products[2].addEventListener("click",function (){
        if (nb!==5) {
            nb++
            products[1].innerHTML=nb
            button_submit.value=nb
        }
        if (nb===5) {
            div_info.appendChild(box_error)
        }
    })
})